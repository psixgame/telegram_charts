package com.psixgame.telegramcharts.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.psixgame.telegramcharts.DataHolder;
import com.psixgame.telegramcharts.MyApplication;
import com.psixgame.telegramcharts.R;
import com.psixgame.telegramcharts.entities.Chart;
import com.psixgame.telegramcharts.parser.ChartsParser;
import com.psixgame.telegramcharts.settings.Settings;
import com.psixgame.telegramcharts.ui.chart_review.ChartReviewFragment;
import com.psixgame.telegramcharts.ui.choose_chart.ChooseChartFragment;
import com.psixgame.telegramcharts.utils.Checks;
import com.psixgame.telegramcharts.utils.FileReader;

import org.json.JSONException;

import java.util.List;

public class MainActivity extends Activity implements Settings.NightModeListener {

    private Settings settings;

    private View rootContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final DataHolder dataHolder = ((MyApplication) getApplication()).getDataHolder();
        settings = ((MyApplication) getApplication()).getSettings();

        rootContainer = findViewById(R.id.rootContainer);

        if (!dataHolder.hasCharts()) {
            //todo: parse in background
            final List<Chart> charts = getCharts();
            Checks.checkNonNull(charts);
            dataHolder.setCharts(charts);
        }

        onNightModeChanged(settings.isNightMode());

        final FragmentManager ft = getFragmentManager();
        if (ft.findFragmentById(R.id.rootContainer) == null) {
            ft.beginTransaction()
                    .add(R.id.rootContainer, ChooseChartFragment.create())
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        settings.addNightModeListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        settings.removeNightModeListener(this);
    }

    private static final String FILE_NAME = "chart_data.json";
    private static final int FILE_SIZE = 27417;

    private List<Chart> getCharts() {
        final FileReader fileReader = new FileReader(getApplicationContext());
        final String str = fileReader.readFromAssetsFile(FILE_NAME, FILE_SIZE);

        Checks.checkFalse(str.isEmpty());
        List<Chart> charts = null;

        try {
            charts = new ChartsParser().parse(str);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return charts;
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        final MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_night_mode:
                onClickMenuNightMode();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onClickMenuNightMode() {
        settings.toggleNightMode();
    }

    @Override
    public final void onNightModeChanged(final boolean enabled) {
        final int statusBarColor = getResources().getColor(enabled ? R.color.status_bar_bg_night : R.color.status_bar_bg_day);
        getWindow().setStatusBarColor(statusBarColor);
        final int toolbarColor = getResources().getColor(enabled ? R.color.toolbar_bg_night : R.color.toolbar_bg_day);
        getActionBar().setBackgroundDrawable(new ColorDrawable(toolbarColor));
        final int bgColor = getResources().getColor(enabled ? R.color.root_bg_night : R.color.root_bg_day);
        rootContainer.setBackgroundColor(bgColor);
    }

    public final void openChart(final int pos) {
        final FragmentManager fm = getFragmentManager();
        fm.beginTransaction()
                .replace(R.id.rootContainer, ChartReviewFragment.create(pos))
                .addToBackStack(null)
                .commit();
    }
}
