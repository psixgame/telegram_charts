package com.psixgame.telegramcharts.ui.chart_review;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.psixgame.telegramcharts.MyApplication;
import com.psixgame.telegramcharts.R;
import com.psixgame.telegramcharts.settings.Settings;

import java.util.List;

public class ChartSelectionView extends View implements Settings.NightModeListener {

    public interface SelectionListener {
        /**
         * Called when selection was changed.
         * Always start < end.
         *
         * @param start value in range 0..1.
         * @param end   value in range 0..1.
         */
        void onSelectionChanged(float start, float end);
    }

    public ChartSelectionView(final Context context) {
        this(context, null);
    }

    public ChartSelectionView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChartSelectionView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ChartSelectionView(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private Settings settings;

    private Paint bgPaint;
    private Paint unselectedBgPaint;
    private Paint selectionBorderPaint;

    private void init() {
        settings = ((MyApplication) getContext().getApplicationContext()).getSettings();
        bgPaint = new Paint();
        unselectedBgPaint = new Paint();
        selectionBorderPaint = new Paint();

        retrieveColors(settings.isNightMode());
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        settings.addNightModeListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        settings.removeNightModeListener(this);
    }

    private static final int borderColor = Color.parseColor("#90DBE7F0");
    private static final int unselectedBgColor = Color.parseColor("#aaF5F8F9");
    private int bgColor;

    private void retrieveColors(final boolean nightMode) {
        bgColor = getContext().getResources().getColor(nightMode ? R.color.view_bg_night : R.color.view_bg_day);
    }

    private void setColors() {
        bgPaint.setColor(bgColor);
        unselectedBgPaint.setColor(unselectedBgColor);
        selectionBorderPaint.setColor(borderColor);
    }

    private SeriesCalculator calculator;
    private SeriesDrawer seriesDrawer = null;

    public final void setSeries(final List<SelectableSeries> series) {
        calculator = new SeriesCalculator();
        calculator.setSeries(series);
        calculator.calculate();

        seriesDrawer = new SeriesDrawer();
        seriesDrawer.setSeriesCalculator(calculator);

        setColors();

        invalidate();
    }

    private boolean rectsCreated = false;
    private Rect seriesRect;

    private Bitmap bg;

    @Override
    protected void onDraw(final Canvas canvas) {
        if (!canDraw()) return;

        if (!rectsCreated) {
            rectsCreated = true;
            createRects();
        }

        if (bg == null) {
            bg = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            final Canvas bgCanvas = new Canvas(bg);
            bgCanvas.drawPaint(bgPaint);
            seriesDrawer.drawSeries(bgCanvas, seriesRect);
        }

        canvas.drawBitmap(bg, 0,0, null);
        drawSelector(canvas);
    }

    private boolean canDraw() {
        return calculator.hasSeries();
    }

    private void createRects() {
        final int width = getWidth();
        final int height = getHeight();
        seriesRect = new Rect(0, 0, width, height);
    }

    private int startSelectionX = -1;
    private int endSelectionX = -1;

    private static final float verticalBorderThickness = 0.03f;
    private static final float horizontalBorderThickness = 0.03f;

    private final RectF startBorder = new RectF();
    private final RectF endBorder = new RectF();
    private final RectF selectionArea = new RectF();

    private void drawSelector(final Canvas canvas) {
        if (startSelectionX == -1) startSelectionX = 0;
        if (endSelectionX == -1) endSelectionX = canvas.getWidth();

        final float selectionRange = endSelectionX - startSelectionX;

        //left line
        startBorder.set(startSelectionX,
                0,
                startSelectionX + verticalBorderThickness * canvas.getWidth(),
                canvas.getHeight());
        canvas.drawRect(startBorder, selectionBorderPaint);

        //right line
        endBorder.set(startSelectionX + selectionRange - verticalBorderThickness * canvas.getWidth(),
                0,
                startSelectionX + selectionRange,
                canvas.getHeight());
        canvas.drawRect(endBorder, selectionBorderPaint);

        //top line
        canvas.drawRect(startSelectionX + verticalBorderThickness * canvas.getWidth(),
                0,
                startSelectionX + selectionRange - verticalBorderThickness * canvas.getWidth(),
                horizontalBorderThickness * canvas.getHeight(),
                selectionBorderPaint);

        //bottom line
        canvas.drawRect(startSelectionX + verticalBorderThickness * canvas.getWidth(),
                canvas.getHeight() - horizontalBorderThickness * canvas.getHeight(),
                startSelectionX + selectionRange - verticalBorderThickness * canvas.getWidth(),
                canvas.getHeight(),
                selectionBorderPaint);

        selectionArea.set(startSelectionX + verticalBorderThickness * canvas.getWidth(),
                0,
                startSelectionX + selectionRange - verticalBorderThickness * canvas.getWidth(),
                canvas.getHeight());

        if (startSelectionX > 0) {
            drawUnselectedBg(canvas, 0, startSelectionX);
        }

        if (endSelectionX < canvas.getWidth()) {
            drawUnselectedBg(canvas, endSelectionX, canvas.getWidth());
        }

    }

    private void drawUnselectedBg(final Canvas canvas, final float startX, final float endX) {
        canvas.drawRect(startX, 0, endX, canvas.getHeight(), unselectedBgPaint);
    }

    private long lastTime = 0;
    private static final double FRAME_TIME = 1000f / 60f;

    private boolean canHandleAction() {
        final long diff = System.currentTimeMillis() - lastTime;
        return diff > FRAME_TIME;
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        if (!canHandleAction()) return super.onTouchEvent(event);
        lastTime = System.currentTimeMillis();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                down(event);
                return true;

            case MotionEvent.ACTION_MOVE:
                moved(event);
                return true;
        }

        return true;
    }

    private static final int CHANGE_TYPE_NONE = 0;
    private static final int CHANGE_TYPE_START_BORDER = 1;
    private static final int CHANGE_TYPE_END_BORDER = 2;
    private static final int CHANGE_TYPE_SELECTION_AREA = 3;

    private int changeType = CHANGE_TYPE_NONE;

    private int downX = -1;
    private int paddingFromStartX = -1;

    private void down(final MotionEvent event) {
        downX = (int) event.getX();

        if (startBorder.contains(downX, event.getY())) {
            changeType = CHANGE_TYPE_START_BORDER;
            paddingFromStartX = downX - startSelectionX;
        } else if (endBorder.contains(downX, event.getY())) {
            changeType = CHANGE_TYPE_END_BORDER;
            paddingFromStartX = downX - endSelectionX;
        } else if (selectionArea.contains(downX, event.getY())) {
            changeType = CHANGE_TYPE_SELECTION_AREA;
            paddingFromStartX = downX - startSelectionX;
        } else {
            changeType = CHANGE_TYPE_NONE;
        }
    }

    private void moved(final MotionEvent event) {
        if (CHANGE_TYPE_NONE == changeType) return;

        final int prevStartX = startSelectionX;
        final int prevEndX = endSelectionX;

        lastTime = System.currentTimeMillis();
        final int x = (int) event.getX();
        final int diff = x - downX;
        final int minSelectionAreaWidth = (int) (getWidth() * 0.1f);

        switch (changeType) {
            case CHANGE_TYPE_START_BORDER:
                int newStartX = startSelectionX + diff;
                if (newStartX > endSelectionX - minSelectionAreaWidth) {
                    newStartX = endSelectionX - minSelectionAreaWidth;
                }
                startSelectionX = Math.max(0, newStartX);
                downX = startSelectionX + paddingFromStartX;
                break;

            case CHANGE_TYPE_END_BORDER:
                int newEndX = endSelectionX + diff;
                if (newEndX < startSelectionX + minSelectionAreaWidth) {
                    newEndX = startSelectionX + minSelectionAreaWidth;
                }
                endSelectionX = Math.min(newEndX, getWidth());
                downX = endSelectionX + paddingFromStartX;
                break;

            case CHANGE_TYPE_SELECTION_AREA:
                final int areaWidth = endSelectionX - startSelectionX;

                final int newStartSelectionX = Math.min(startSelectionX + diff, getWidth() - areaWidth);
                startSelectionX = Math.max(0, newStartSelectionX);

                final int newEndSelectionX = Math.max(endSelectionX + diff, areaWidth);
                endSelectionX = Math.min(getWidth(), newEndSelectionX);

                downX = startSelectionX + paddingFromStartX;
                break;
        }

        if (startSelectionX != prevStartX || endSelectionX != prevEndX) {
            notifySelectionListener();
            invalidate();
        }
    }

    private SelectionListener selectionListener;

    public void setSelectionListener(final SelectionListener selectionListener) {
        this.selectionListener = selectionListener;
    }

    private void notifySelectionListener() {
        if (selectionListener != null) {
            final float startChange = startSelectionX / (float) getWidth();
            final float endChange = endSelectionX / (float) getWidth();
            selectionListener.onSelectionChanged(startChange, endChange);
        }
    }

    @Override
    public final void onNightModeChanged(final boolean enabled) {
        bg = null;
        retrieveColors(enabled);
        setColors();
        invalidate();
    }

    /**
     * Should be called when data set is changed (i.e. series selected/unselected).
     */
    public final void reset() {
        bg = null;
        calculator.calculate();
    }

}
