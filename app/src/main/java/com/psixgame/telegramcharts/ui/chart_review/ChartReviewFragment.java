package com.psixgame.telegramcharts.ui.chart_review;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.psixgame.telegramcharts.DataHolder;
import com.psixgame.telegramcharts.MyApplication;
import com.psixgame.telegramcharts.R;
import com.psixgame.telegramcharts.entities.Series;
import com.psixgame.telegramcharts.settings.Settings;

import java.util.ArrayList;
import java.util.List;

public final class ChartReviewFragment extends Fragment implements Settings.NightModeListener, CompoundButton.OnCheckedChangeListener {

    private static final String KEY_CHART_POSITION = "key_chart_position";

    public static ChartReviewFragment create(final int chartPos) {
        final Bundle bundle = new Bundle();
        bundle.putInt(KEY_CHART_POSITION, chartPos);
        final ChartReviewFragment fragment = new ChartReviewFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private DataHolder dataHolder;
    private Settings settings;

    @Override
    public void onAttach(final Activity context) {
        super.onAttach(context);
        settings = ((MyApplication) getActivity().getApplication()).getSettings();
        dataHolder = ((MyApplication) getActivity().getApplication()).getDataHolder();
    }

    private LinearLayout rootContainer;
    private LinearLayout chartContainer;
    private ChartView chartView;
    private ChartSelectionView chartSelectionView;
    private LinearLayout listContainer;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        init();

        final View view = inflater.inflate(R.layout.fragment_chart_review, container, false);
        rootContainer = view.findViewById(R.id.rootContainer);
        chartContainer = view.findViewById(R.id.chartContainer);
        chartView = view.findViewById(R.id.chartView);
        chartSelectionView = view.findViewById(R.id.chartSelectionView);
        listContainer = view.findViewById(R.id.listContainer);
        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setBgColor(settings.isNightMode());

        chartSelectionView.setSelectionListener(chartView);
        chartView.setSeries(selectableSeries);
        chartSelectionView.setSeries(selectableSeries);

        for (int i = 0; i < selectableSeries.size(); i++) {
            addCheckBox(i, selectableSeries.get(i));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        settings.addNightModeListener(this);
        settings.addNightModeListener(chartView);
        settings.addNightModeListener(chartSelectionView);
    }

    @Override
    public void onStop() {
        super.onStop();
        settings.removeNightModeListener(this);
        settings.removeNightModeListener(chartView);
        settings.removeNightModeListener(chartSelectionView);
    }

    private List<SelectableSeries> selectableSeries;

    private void init() {
        selectableSeries = createSelectableSeries(dataHolder.getCharts().get(getArguments().getInt(KEY_CHART_POSITION, -1)).getSeries());
    }

    private List<SelectableSeries> createSelectableSeries(final List<Series> series) {
        final List<SelectableSeries> selectableSeries = new ArrayList<>(series.size());
        for (final Series item : series) {
            selectableSeries.add(new SelectableSeries(item));
        }
        return selectableSeries;
    }

    private void addCheckBox(final int tag, final SelectableSeries selectableSeries) {
        final Series series = selectableSeries.getSeries();

        final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        final int margin = (int) getResources().getDimension(R.dimen.size_2dp);
        layoutParams.setMargins(0, 0, 0, margin);
        final CheckBox checkBox = new CheckBox(getActivity());
        checkBox.setTag(tag);
        checkBox.setLayoutParams(layoutParams);
        final int padding = (int) getResources().getDimension(R.dimen.size_8dp);
        checkBox.setPadding(padding, padding, padding, padding);
        final boolean night = settings.isNightMode();
        checkBox.setBackgroundColor(getViewBgColor(night));
        checkBox.setTextColor(getListItemTextColor(night));
        checkBox.setText(series.getName());
        checkBox.setChecked(selectableSeries.isSelected());
        checkBox.setButtonTintList(ColorStateList.valueOf(Color.parseColor(series.getColor())));
        checkBox.setOnCheckedChangeListener(this);

        listContainer.addView(checkBox);
    }

    @Override
    public void onNightModeChanged(final boolean enabled) {
        setBgColor(enabled);
        setItemsColor(enabled);
    }

    private void setBgColor(final boolean enabled) {
        final int bgColor = getResources().getColor(enabled ? R.color.root_bg_night : R.color.root_bg_day);
        rootContainer.setBackgroundColor(bgColor);
        chartContainer.setBackgroundColor(getViewBgColor(enabled));
    }

    private void setItemsColor(final boolean night) {
        final int bgColor = getViewBgColor(night);
        final int textColor = getListItemTextColor(night);
        for (int i = 0; i < listContainer.getChildCount(); i++) {
            final View child = listContainer.getChildAt(i);
            if (child instanceof CheckBox) {
                child.setBackgroundColor(bgColor);
                ((CheckBox) child).setTextColor(textColor);
            }
        }
    }

    private int getViewBgColor(final boolean enabled) {
        return getResources().getColor(enabled ? R.color.view_bg_night : R.color.view_bg_day);
    }

    private int getListItemTextColor(final boolean enabled) {
        return getResources().getColor(enabled ? R.color.general_text_night : R.color.general_text_day);
    }

    @Override
    public final void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
        selectableSeries.get(((int) buttonView.getTag())).setSelected(isChecked);
        chartView.reset();
        chartView.invalidate();
        chartSelectionView.reset();
        chartSelectionView.invalidate();
    }
}
