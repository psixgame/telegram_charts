package com.psixgame.telegramcharts.ui.chart_review;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Size;

import com.psixgame.telegramcharts.entities.Series;
import com.psixgame.telegramcharts.entities.SeriesItem;

import java.util.List;

final class SeriesDrawer {

    private final Paint paint;

    public SeriesDrawer() {
        paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeJoin(Paint.Join.ROUND);
    }

    private SeriesCalculator calculator;

    public void setSeriesCalculator(final SeriesCalculator seriesCalculator) {
        this.calculator = seriesCalculator;
    }

    final void drawSeries(final Canvas canvas, final Rect rect) {
        calculator.setDrawSize(new Size(rect.width(), rect.height()));

        for (final SelectableSeries series : calculator.getSeries()) {
            if (series.isSelected()) {
                draw(canvas, series.getSeries());
            }
        }
    }

    private void draw(final Canvas canvas, final Series series) {
        paint.setColor(Color.parseColor(series.getColor()));
        final List<SeriesItem> items = series.getItems();

        final int firstPos = calculator.getFirstItemPosition(items);
        final int lastPos = calculator.getLastItemPosition(items);
        final int count = lastPos - firstPos + 1;

        final SeriesItem first = items.get(firstPos);
        final SeriesItem last = items.get(lastPos);

        final float[] points = new float[(count - 2) * 4 + 4];

        points[0] = calculateDrawX(first);
        points[1] = calculateDrawY(first);

        for (int i = firstPos + 1, k = 2; i < lastPos; i++, k += 4) {
            final SeriesItem item = items.get(i);
            final float x = calculateDrawX(item);
            final float y = calculateDrawY(item);
            points[k] = x;
            points[k + 1] = y;
            points[k + 2] = x;
            points[k + 3] = y;
        }

        points[(count - 2) * 4 + 2] = calculateDrawX(last);
        points[(count - 2) * 4 + 3] = calculateDrawY(last);

        canvas.drawLines(points, paint);
    }

    private float calculateDrawX(final SeriesItem item) {
        return calculator.getDrawX(item.getX());
    }

    private float calculateDrawY(final SeriesItem item) {
        return calculator.getDrawY(item.getY());
    }
}
