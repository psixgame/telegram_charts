package com.psixgame.telegramcharts.ui.chart_review.popup;

import com.psixgame.telegramcharts.entities.SeriesItem;

public final class PopupItem {

    private final SeriesItem seriesItem;
    private final String text;
    private final int color;

    public PopupItem(final SeriesItem seriesItem, final String text, final int color) {
        this.seriesItem = seriesItem;
        this.text = text;
        this.color = color;
    }

    public final SeriesItem getSeriesItem() {
        return seriesItem;
    }

    public final String getText() {
        return text;
    }

    public final int getColor() {
        return color;
    }

    @Override
    public final String toString() {
        return "PopupItem{" +
                "seriesItem=" + seriesItem +
                ", text='" + text + '\'' +
                ", color=" + color +
                '}';
    }
}
