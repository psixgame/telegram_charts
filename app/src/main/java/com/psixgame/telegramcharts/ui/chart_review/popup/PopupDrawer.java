package com.psixgame.telegramcharts.ui.chart_review.popup;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.Size;
import android.view.MotionEvent;

import com.psixgame.telegramcharts.entities.SeriesItem;
import com.psixgame.telegramcharts.ui.chart_review.SelectableSeries;
import com.psixgame.telegramcharts.ui.chart_review.SeriesCalculator;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public final class PopupDrawer {

    private final Paint bgPaint;
    private final Paint textPaint;
    private final Paint linePaint;
    private final Paint circlePaint;

    public PopupDrawer() {
        bgPaint = new Paint();
        bgPaint.setStrokeWidth(6);
        bgPaint.setAntiAlias(true);
        textPaint = new Paint();
        textPaint.setAntiAlias(true);
        linePaint = new Paint();
        linePaint.setStrokeWidth(3);
        circlePaint = new Paint();
        circlePaint.setStrokeWidth(5);
        circlePaint.setAntiAlias(true);
    }

    private int bgColor = -1;

    public final void setBgColor(final int color) {
        bgColor = color;
    }

    private int headerTextColor = -1;

    public final void setHeaderTextColor(final int color) {
        headerTextColor = color;
    }

    private int borderColor = -1;

    public final void setBorderColor(final int color) {
        borderColor = color;
    }

    public final void setLineColor(final int color) {
        linePaint.setColor(color);
    }

    private int circleBgColor = -1;

    public final void setCircleBgColor(final int color) {
        circleBgColor = color;
    }

    private SeriesCalculator calculator;

    public final void setSeriesCalculator(final SeriesCalculator seriesCalculator) {
        this.calculator = seriesCalculator;
    }

    private Rect rect;

    public final void setRect(final Rect rect) {
        this.rect = rect;
    }

    public boolean onTouchEvent(final MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                down(event);
                return true;

            case MotionEvent.ACTION_MOVE:
                return moved(event);

            case MotionEvent.ACTION_UP:
                return up();
        }
        return false;
    }

    private boolean active = false;

    private void down(final MotionEvent event) {
        active = true;
        calculateSelectedPosition(event);
    }

    private boolean moved(final MotionEvent event) {
        if (!active) return false;
        calculateSelectedPosition(event);
        return true;
    }

    private int selectedPosition = -1;

    private void calculateSelectedPosition(final MotionEvent event) {
        final float x = event.getX();
        final float scaleX = calculator.getSelectionStartX() + x / rect.width() * calculator.getSelectionScale();
        final int size = calculator.getSeries().get(0).getSeries().getItems().size();
        selectedPosition = Math.round((size - 1) * scaleX);
        if (selectedPosition < 0) {
            selectedPosition = 0;
        } else if (selectedPosition > size - 1) {
            selectedPosition = size - 1;
        }
    }

    private boolean up() {
        if (!active) return false;
        active = false;
        return true;
    }

    private final List<PopupItem> popupItems = new ArrayList<>();
    private final RectF popupRect = new RectF();

    public final void draw(final Canvas canvas) {
        if (selectedPosition == -1) return;

        long millis = -1;
        String header = null;

        popupItems.clear();

        for (final SelectableSeries series : calculator.getSeries()) {
            if (!series.isSelected()) continue;

            final SeriesItem item = series.getSeries().getItems().get(selectedPosition);

            if (millis == -1) {
                millis = item.getX();
                header = formatMillisToDateText(millis);
            }

            popupItems.add(new PopupItem(item, series.getSeries().getName() + " " + item.getY(), Color.parseColor(series.getSeries().getColor())));
        }

        final int lineX = calculator.getDrawX(millis);
        canvas.drawLine(lineX, 0, lineX, rect.height(), linePaint);

        final Size size = new Size(rect.width(), rect.height());
        calculator.setDrawSize(size);

        for (final PopupItem item : popupItems) {
            circlePaint.setStyle(Paint.Style.STROKE);
            circlePaint.setColor(item.getColor());

            final int itemY = calculator.getDrawY(item.getSeriesItem().getY());
            canvas.drawCircle(lineX, itemY, rect.width() * 0.012f, circlePaint);

            circlePaint.setStyle(Paint.Style.FILL);
            circlePaint.setColor(circleBgColor);
            canvas.drawCircle(lineX, itemY, rect.width() * 0.01f, circlePaint);
        }

        final float padding = rect.height() * 0.03f;
        final float headerTextPadding = rect.height() * 0.02f;
        final float itemTextPadding = rect.height() * 0.01f;
        final float headerTextHeight = rect.height() * 0.06f;
        final float itemTextHeight = rect.height() * 0.05f;

        textPaint.setTextSize(headerTextHeight);
        float textWidth = textPaint.measureText(header);
        textPaint.setTextSize(itemTextHeight);
        for (final PopupItem item : popupItems) {
            textWidth = Math.max(textWidth, textPaint.measureText(item.getText()));
        }

        final float popupHeight = padding + headerTextHeight + headerTextPadding + itemTextHeight * popupItems.size() + itemTextPadding * (popupItems.size() - 1) + padding;
        final float popupWidth = padding * 2 + textWidth;

        final float popupMargin = rect.height() * 0.05f;
        final float popupWidthWithMargins = popupWidth + 2 * popupMargin;

        final float popupLeft = rect.width() - lineX > popupWidthWithMargins ? lineX + popupMargin : lineX - popupMargin - popupWidth;
        final float popupTop = popupMargin;
        final float popupRight = popupLeft + popupWidth;
        final float popupBottom = popupTop + popupHeight;

        popupRect.set(popupLeft, popupTop, popupRight, popupBottom);
        final float popupRoundingRadius = popupMargin / 2f;

        bgPaint.setStyle(Paint.Style.STROKE);
        bgPaint.setColor(borderColor);
        canvas.drawRoundRect(popupRect, popupRoundingRadius, popupRoundingRadius, bgPaint);
        bgPaint.setStyle(Paint.Style.FILL);
        bgPaint.setColor(bgColor);
        canvas.drawRoundRect(popupRect, popupRoundingRadius, popupRoundingRadius, bgPaint);

        final float textX = popupLeft + padding;
        float textY = popupTop + padding + headerTextHeight;

        textPaint.setTextSize(headerTextHeight);
        textPaint.setColor(headerTextColor);
        canvas.drawText(header, textX, textY, textPaint);

        textY = textY + headerTextPadding + itemTextHeight;
        textPaint.setTextSize(itemTextHeight);
        for (final PopupItem item : popupItems) {
            textPaint.setColor(item.getColor());
            canvas.drawText(item.getText(), textX, textY, textPaint);
            textY = textY + itemTextHeight + itemTextPadding;
        }
    }

    private final SimpleDateFormat formatter = new SimpleDateFormat("EEE, MMM d", Locale.US);
    private final Calendar calendar = Calendar.getInstance();

    private String formatMillisToDateText(final long millis) {
        calendar.setTimeInMillis(millis);
        return formatter.format(calendar.getTime());
    }

}
