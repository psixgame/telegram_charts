package com.psixgame.telegramcharts.ui.chart_review;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Size;

import com.psixgame.telegramcharts.entities.SeriesItem;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by vasiliy.barchiy on 3/21/2019.
 */
public class DaysDrawer {

    private final Paint textPaint;

    public DaysDrawer() {
        textPaint = new Paint();
        textPaint.setAntiAlias(true);
    }

    public void setTextColor(final int textColor) {
        textPaint.setColor(textColor);
    }

    private SeriesCalculator calculator;

    public void setSeriesCalculator(final SeriesCalculator seriesCalculator) {
        this.calculator = seriesCalculator;
    }

    public final void drawDays(final Canvas canvas, final Rect rect) {
        final Size size = new Size(rect.width(), rect.height());
        calculator.setDrawSize(size);
        textPaint.setTextSize(size.getHeight() * 0.5f);

        final float dayTextWidth = textPaint.measureText("MMM 00");
        final int totalDaysToDraw = (int) (Math.ceil(rect.width() / dayTextWidth / 2f / calculator.getSelectionScale()));
        final List<SeriesItem> items = calculator.getSeries().get(0).getSeries().getItems();
        final int count = items.size();
        final int step = count / totalDaysToDraw;

        final int startPos = (int) Math.floor((count - 1) * calculator.getSelectionStartX());
        final int endPos = (int) Math.floor((count - 1) * calculator.getSelectionEndX());

        for (int i = 0; i < count - 1; i += step) {
            if (i < startPos) continue;
            if (i > endPos) return;
            canvas.drawText(formatMillisToDateText(items.get(i).getX()),
                    calculator.getDrawX(items.get(i).getX()),
                    ((size.getHeight() / 2f) - ((textPaint.descent() + textPaint.ascent()) / 2)),
                    textPaint);
        }
    }

    private String formatMillisToDateText(final long millis) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        final SimpleDateFormat formatter = new SimpleDateFormat("MMM d", Locale.US);
        return formatter.format(calendar.getTime());
    }
}
