package com.psixgame.telegramcharts.ui.chart_review;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Size;

public class GridDrawer {

    private final Paint linePaint;
    private final Paint textPaint;

    public GridDrawer() {
        linePaint = new Paint();
        linePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        linePaint.setStrokeWidth(2);

        textPaint = new Paint();
        textPaint.setAntiAlias(true);
    }

    public void setLineColor(final int lineColor) {
        linePaint.setColor(lineColor);
    }

    public void setTextColor(final int textColor) {
        textPaint.setColor(textColor);
    }

    private SeriesCalculator calculator;

    public void setSeriesCalculator(final SeriesCalculator seriesCalculator) {
        this.calculator = seriesCalculator;
    }

    public final void drawLines(final Canvas canvas, final Rect rect) {
        calculator.setDrawSize(new Size(rect.width(), rect.height()));

        final float pixelsStepY = calculator.getStepYPixels();

        final float bottom = rect.height();
        float y;
        for (int i = 0; i < calculator.getStepsY(); i++) {
            y = bottom - pixelsStepY * i;
            canvas.drawLine(0, y, rect.width(), y, linePaint);
        }
    }

    public final void drawValues(final Canvas canvas, final Rect rect) {
        calculator.setDrawSize(new Size(rect.width(), rect.height()));
        textPaint.setTextSize(rect.height() * 0.05f);

        final long dataStepY = calculator.getStepY();
        final float pixelsStepY = calculator.getStepYPixels();

        final float textBottomPadding = getTextPaddingY(rect);

        final long min = calculator.getMinY();

        final float bottom = rect.height() - textBottomPadding;

        for (int i = 0; i < calculator.getStepsY(); i++) {
            canvas.drawText(min + dataStepY * i + "", 0, bottom - pixelsStepY * i, textPaint);
        }
    }

    private float getTextPaddingY(final Rect rect) {
        return Math.min(rect.width(), rect.height()) * 0.015f;
    }
}
