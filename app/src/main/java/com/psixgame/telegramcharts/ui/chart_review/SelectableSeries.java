package com.psixgame.telegramcharts.ui.chart_review;

import com.psixgame.telegramcharts.entities.Series;

public class SelectableSeries {

    private final Series series;
    private boolean selected = true;

    public SelectableSeries(final Series series) {
        this.series = series;
    }

    public Series getSeries() {
        return series;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }

    @Override
    public final String toString() {
        return "SelectableSeries{" +
                "series=" + series +
                ", selected=" + selected +
                '}';
    }
}
