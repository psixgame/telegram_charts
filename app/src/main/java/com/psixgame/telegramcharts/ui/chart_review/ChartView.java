package com.psixgame.telegramcharts.ui.chart_review;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.psixgame.telegramcharts.MyApplication;
import com.psixgame.telegramcharts.R;
import com.psixgame.telegramcharts.settings.Settings;
import com.psixgame.telegramcharts.ui.chart_review.popup.PopupDrawer;

import java.util.List;

public final class ChartView extends View implements ChartSelectionView.SelectionListener, Settings.NightModeListener {

    public ChartView(final Context context) {
        this(context, null);
    }

    public ChartView(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ChartView(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ChartView(final Context context, final AttributeSet attrs, final int defStyleAttr, final int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private Settings settings;

    private void init() {
        settings = ((MyApplication) getContext().getApplicationContext()).getSettings();
        retrieveColors(settings.isNightMode());
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        settings.addNightModeListener(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        settings.removeNightModeListener(this);
    }

    private int bgColor;
    private int gridLineColor;
    private int chartTextColor;
    private int popupBgColor;
    private int popupHeaderTextColor;
    private int popupBorderColor;
    private int popupLineColor;
    private int popupCircleBgColor;

    private void retrieveColors(final boolean nightMode) {
        bgColor = getContext().getResources().getColor(nightMode ? R.color.view_bg_night : R.color.view_bg_day);
        gridLineColor = getContext().getResources().getColor(nightMode ? R.color.grid_line_night : R.color.grid_line_day);
        chartTextColor = getContext().getResources().getColor(nightMode ? R.color.chart_text_night : R.color.chart_text_day);
        popupBgColor = getContext().getResources().getColor(nightMode ? R.color.popup_bg_night : R.color.popup_bg_day);
        popupHeaderTextColor = getContext().getResources().getColor(nightMode ? R.color.general_text_night : R.color.general_text_day);
        popupBorderColor = getContext().getResources().getColor(nightMode ? R.color.popup_border_color_night : R.color.popup_border_color_day);
        popupLineColor = getContext().getResources().getColor(nightMode ? R.color.popup_line_color_night : R.color.popup_line_color_day);
        popupCircleBgColor = getContext().getResources().getColor(nightMode ? R.color.view_bg_night : R.color.view_bg_day);
    }

    private void setColors() {
        bgPaint.setColor(bgColor);
        gridDrawer.setLineColor(gridLineColor);
        gridDrawer.setTextColor(chartTextColor);
        daysDrawer.setTextColor(chartTextColor);
        popupDrawer.setBgColor(popupBgColor);
        popupDrawer.setHeaderTextColor(popupHeaderTextColor);
        popupDrawer.setBorderColor(popupBorderColor);
        popupDrawer.setLineColor(popupLineColor);
        popupDrawer.setCircleBgColor(popupCircleBgColor);
    }

    private SeriesCalculator calculator;
    private Paint bgPaint;
    private Paint alphaPaint;

    private SeriesDrawer seriesDrawer = null;
    private GridDrawer gridDrawer = null;
    private DaysDrawer daysDrawer = null;
    private PopupDrawer popupDrawer = null;

    public final void setSeries(final List<SelectableSeries> series) {
        bgPaint = new Paint();
        alphaPaint = new Paint();

        calculator = new SeriesCalculator();
        calculator.setSeries(series);
        calculator.calculate();

        seriesDrawer = new SeriesDrawer();
        seriesDrawer.setSeriesCalculator(calculator);

        gridDrawer = new GridDrawer();
        gridDrawer.setSeriesCalculator(calculator);

        daysDrawer = new DaysDrawer();
        daysDrawer.setSeriesCalculator(calculator);

        popupDrawer = new PopupDrawer();
        popupDrawer.setSeriesCalculator(calculator);

        setColors();

        invalidate();
    }

    private boolean rectsCreated = false;
    private Rect chartRect;
    private Rect daysRect;

    private static final long DURATION = 100;

    private static final String SCALE = "scale";
    private static final String ALPHA = "alpha";

    private Canvas intermediateCanvas;

    private long lastMinData = -1;
    private long astMaxData = -1;

    private boolean animate = false;

    private Bitmap disappear;
    private float scaleDisappear;
    private int alphaDisappear;

    private Bitmap appear;
    private float scaleAppear;
    private int alphaAppear;

    @Override
    protected void onDraw(final Canvas canvas) {
        if (!canDraw()) return;

        canvas.drawPaint(bgPaint);

        if (!calculator.hasSelectedSeries()) return;

        if (!rectsCreated) {
            rectsCreated = true;
            createRects();
            popupDrawer.setRect(chartRect);
        }

        final long min = calculator.getMinY();
        final long max = calculator.getMaxY();

        if (appear == null)
            appear = Bitmap.createBitmap(chartRect.width(), chartRect.height(), Bitmap.Config.ARGB_8888);
        if (intermediateCanvas == null) intermediateCanvas = new Canvas(appear);

        final boolean extremesChanged = extremesChanged(lastMinData, astMaxData, min, max);
        float scaleToDisappear = -1;
        float scaleToAppear = -1;

        if (extremesChanged) {
            disappear = appear.copy(appear.getConfig(), appear.isMutable());
            appear.eraseColor(Color.TRANSPARENT);
            animate = true;
            scaleToDisappear = (float) (astMaxData - lastMinData) / max;
            scaleToAppear = (float) max / (astMaxData - lastMinData);
        } else {
            appear.eraseColor(Color.TRANSPARENT);
        }

        gridDrawer.drawLines(intermediateCanvas, chartRect);
        seriesDrawer.drawSeries(intermediateCanvas, chartRect);
        gridDrawer.drawValues(intermediateCanvas, chartRect);
        popupDrawer.draw(intermediateCanvas);

        if (extremesChanged) {
            animateLinesDisappear(scaleToDisappear);
            animateLinesAppear(scaleToAppear);
        }

        if (animate) {
            alphaPaint.setAlpha(alphaDisappear);
            canvas.save();
            canvas.scale(1, scaleDisappear, 0, chartRect.bottom);
            canvas.drawBitmap(disappear, chartRect.left, chartRect.top, alphaPaint);
            canvas.restore();

            alphaPaint.setAlpha(alphaAppear);
            canvas.save();
            canvas.scale(1, scaleAppear, 0, chartRect.bottom);
            canvas.drawBitmap(appear, chartRect.left, chartRect.top, alphaPaint);
            canvas.restore();
        } else {
            canvas.drawBitmap(appear, chartRect.left, chartRect.top, null);
        }

        lastMinData = min;
        astMaxData = max;

        canvas.save();
        canvas.translate(0, daysRect.top);
        daysDrawer.drawDays(canvas, daysRect);
        canvas.restore();
    }

    private boolean canDraw() {
        return calculator.hasSeries();
    }

    private void createRects() {
        final int width = getWidth();
        final int height = getHeight();
        final int daysHeight = (int) (height * 0.085f);
        chartRect = new Rect(0, 0, width, height - daysHeight);
        daysRect = new Rect(0, height - daysHeight, width, height);
    }

    private void animateLinesDisappear(final float scaleTo) {
        final PropertyValuesHolder scaleProp = PropertyValuesHolder.ofFloat(SCALE, 1, scaleTo);
        final PropertyValuesHolder alphaProp = PropertyValuesHolder.ofInt(ALPHA, 225, 0);
        final ValueAnimator animatorDisappear = ValueAnimator.ofPropertyValuesHolder(scaleProp, alphaProp);
        animatorDisappear.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(final ValueAnimator animation) {
                scaleDisappear = (float) animation.getAnimatedValue(SCALE);
                alphaDisappear = (int) animation.getAnimatedValue(ALPHA);
                postInvalidateDelayed(20);
            }
        });
        animatorDisappear.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(final Animator animation) {
                animate = false;
                postInvalidate();
            }
        });
        animatorDisappear.setInterpolator(new LinearInterpolator());
        animatorDisappear.setDuration(DURATION);
        animatorDisappear.start();
    }

    private void animateLinesAppear(final float scaleTo) {
        final PropertyValuesHolder scaleProp = PropertyValuesHolder.ofFloat(SCALE, scaleTo, 1);
        final PropertyValuesHolder alphaProp = PropertyValuesHolder.ofInt(ALPHA, 0, 255);
        final ValueAnimator animatorAppear = ValueAnimator.ofPropertyValuesHolder(scaleProp, alphaProp);
        animatorAppear.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(final ValueAnimator animation) {
                scaleAppear = (float) animation.getAnimatedValue(SCALE);
                alphaAppear = (int) animation.getAnimatedValue(ALPHA);
                postInvalidateDelayed(20);
            }
        });
        animatorAppear.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(final Animator animation) {
                animate = false;
                postInvalidate();
            }
        });
        animatorAppear.setInterpolator(new LinearInterpolator());
        animatorAppear.setDuration(DURATION);
        animatorAppear.start();
    }

    private boolean extremesChanged(final long lastMin, final long lastMax, final long currMin, final long currMax) {
        return (lastMin != -1 && lastMax != -1 && (currMin != lastMin || currMax != lastMax));
    }

    @Override
    public void onSelectionChanged(final float start, final float end) {
        calculator.setSelectionRangeX(start, end);
        calculator.calculate();

        if (!animate) {
            invalidate();
        }
    }

    @Override
    public final void onNightModeChanged(final boolean enabled) {
        retrieveColors(enabled);
        setColors();
        invalidate();
    }

    /**
     * Should be called when data set is changed (i.e. series selected/unselected).
     */
    public final void reset() {
        calculator.calculate();
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        if (!canHandleAction()) return super.onTouchEvent(event);
        lastTime = System.currentTimeMillis();

        if (popupDrawer.onTouchEvent(event)) {
            invalidate();
            return true;
        }
        return super.onTouchEvent(event);
    }

    private long lastTime = 0;
    private static final double FRAME_TIME = 1000f / 60f;

    private boolean canHandleAction() {
        final long diff = System.currentTimeMillis() - lastTime;
        return diff > FRAME_TIME;
    }
}
