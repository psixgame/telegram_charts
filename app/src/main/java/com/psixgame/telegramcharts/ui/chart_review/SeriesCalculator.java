package com.psixgame.telegramcharts.ui.chart_review;

import android.util.Size;

import com.psixgame.telegramcharts.entities.SeriesItem;

import java.util.List;

//todo: rename to processor
public final class SeriesCalculator {

    private List<SelectableSeries> series;

    public final void setSeries(final List<SelectableSeries> series) {
        this.series = series;
    }

    public final List<SelectableSeries> getSeries() {
        return series;
    }

    public final boolean hasSeries() {
        return series != null;
    }

    public final boolean hasSelectedSeries() {
        for (final SelectableSeries series : series) {
            if (series.isSelected()) {
                return true;
            }
        }
        return false;
    }

    public final void calculate() {
        if (!hasSelectedSeries()) return;

        retrieveExtremeValues();
        calculateDataX();
        calculateDataY();
    }

    private long minX;
    private long maxX;
    private long minY;
    private long maxY;

    public final long getMinY() {
        return minY;
    }

    public long getMaxY() {
        return maxY;
    }

    private void retrieveExtremeValues() {
        boolean first = true;

        for (final SelectableSeries series : series) {
            if (!series.isSelected()) continue;

            if (first) {
                first = false;
                minX = maxX = series.getSeries().getItems().get(0).getX();
            }

            for (int i = 1; i < series.getSeries().getItems().size(); i++) {
                final SeriesItem item = series.getSeries().getItems().get(i);
                minX = Math.min(minX, item.getX());
                maxX = Math.max(maxX, item.getX());
            }
        }

        first = true;

        for (final SelectableSeries series : series) {
            if (!series.isSelected()) continue;
            final int firstPos = getFirstItemPosition(series.getSeries().getItems());
            final int lastPos = getLastItemPosition(series.getSeries().getItems());

            if (first) {
                first = false;
                minY = maxY = series.getSeries().getItems().get(firstPos).getY();
            }

            for (int i = firstPos; i <= lastPos; i++) {
                final SeriesItem item = series.getSeries().getItems().get(i);
                minY = Math.min(minY, item.getY());
                maxY = Math.max(maxY, item.getY());
            }
        }
    }

    private long rangeX;

    private void calculateDataX() {
        rangeX = maxX - minX;
    }

    private long stepY;

    public long getStepY() {
        return stepY;
    }

    public int getStepYPixels() {
        return (int) (stepY * scaleY);
    }

    private static final int DESIRED_STEPS_Y = 6;

    private long rangeY;

    private long stepsY;

    public long getStepsY() {
        return stepsY;
    }

    private void calculateDataY() {
        final long rangeY = maxY - minY;
        final long niceRangeY = niceNumber(rangeY, true);
        stepY = niceNumber(niceRangeY / (DESIRED_STEPS_Y - 1), false);
        minY = (long) (Math.floor(minY / (float) stepY) * stepY);
        maxY = (long) (Math.ceil(maxY / (float) stepY) * stepY);
        this.rangeY = maxY - minY;
        stepsY = (maxY - minY) / stepY;
    }

    private long niceNumber(final long number, final boolean round) {
        final int exponent = (int) Math.floor(Math.log10(number));
        final double fraction = number / Math.pow(10, exponent);

        double niceFraction;

        if (round) {
            if (fraction < 1.5) {
                niceFraction = 1.0;
            } else if (fraction < 3.0) {
                niceFraction = 2.0;
            } else if (fraction < 7.0) {
                niceFraction = 5.0;
            } else {
                niceFraction = 10.0;
            }
        } else {
            if (fraction <= 1.0) {
                niceFraction = 1.0;
            } else if (fraction <= 2.0) {
                niceFraction = 2.0;
            } else if (fraction <= 5.0) {
                niceFraction = 5.0;
            } else {
                niceFraction = 10.0;
            }
        }

        return (long) (niceFraction * Math.pow(10, exponent));
    }

    private Size size;

    public final void setDrawSize(final Size size) {
        this.size = size;
        calculateScales();
    }

    private float scaleX;
    private float scaleY;

    private void calculateScales() {
        scaleX = (float) size.getWidth() / rangeX;
        scaleY = (float) size.getHeight() / rangeY;
    }

    public final int getDrawX(final long dataX) {
        return (int) ((((dataX - (minX + selectionStartX * rangeX)) / (selectionEndX - selectionStartX)) * scaleX));
    }

    public final int getDrawY(final long dataY) {
        return (int) (size.getHeight() - (dataY - minY) * scaleY);
    }

    private float selectionStartX = 0;
    private float selectionEndX = 1;

    public final void setSelectionRangeX(final float start, final float end) {
        selectionStartX = start;
        selectionEndX = end;
    }

    public final float getSelectionScale() {
        return selectionEndX - selectionStartX;
    }

    public float getSelectionStartX() {
        return selectionStartX;
    }

    public float getSelectionEndX() {
        return selectionEndX;
    }

    public final int getFirstItemPosition(final List<SeriesItem> items) {
        return (int) Math.floor((items.size() - 1) * selectionStartX);
    }

    public final int getLastItemPosition(final List<SeriesItem> items) {
        return (int) Math.ceil((items.size() - 1) * selectionEndX);
    }
}
