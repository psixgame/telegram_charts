package com.psixgame.telegramcharts.ui.choose_chart;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.psixgame.telegramcharts.DataHolder;
import com.psixgame.telegramcharts.MyApplication;
import com.psixgame.telegramcharts.R;
import com.psixgame.telegramcharts.settings.Settings;
import com.psixgame.telegramcharts.ui.MainActivity;


public final class ChooseChartFragment extends Fragment implements View.OnClickListener, Settings.NightModeListener {

    public static ChooseChartFragment create() {
        return new ChooseChartFragment();
    }

    private DataHolder dataHolder;
    private Settings settings;

    @Override
    public void onAttach(final Activity context) {
        super.onAttach(context);
        settings = ((MyApplication) getActivity().getApplication()).getSettings();
        dataHolder = ((MyApplication) getActivity().getApplication()).getDataHolder();
    }

    private LinearLayout rootContainer;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_choose_chart, container, false);
        rootContainer = view.findViewById(R.id.rootContainer);
        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setBgColor(settings.isNightMode());

        for (int i = 0; i < dataHolder.getCharts().size(); i++) {
            addTextView(i);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        settings.addNightModeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        settings.removeNightModeListener(this);
    }

    private void addTextView(final int tag) {
        final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        final int margin = (int) getResources().getDimension(R.dimen.size_2dp);
        layoutParams.setMargins(0, 0, 0, margin);
        final TextView textView = new TextView(getActivity());
        textView.setTag(tag);
        textView.setLayoutParams(layoutParams);
        final int padding = (int) getResources().getDimension(R.dimen.size_8dp);
        textView.setPadding(padding, padding, padding, padding);
        final boolean night = settings.isNightMode();
        textView.setBackgroundColor(getViewBgColor(night));
        textView.setTextColor(getListItemTextColor(night));
        textView.setText("Chart #" + (tag + 1));
        textView.setOnClickListener(this);

        rootContainer.addView(textView);
    }

    @Override
    public void onNightModeChanged(final boolean enabled) {
        setBgColor(enabled);
        setItemsColor(enabled);
    }

    private void setBgColor(final boolean enabled) {
        final int bgColor = getResources().getColor(enabled ? R.color.root_bg_night : R.color.root_bg_day);
        rootContainer.setBackgroundColor(bgColor);
    }

    private void setItemsColor(final boolean enabled) {
        final int bgColor = getViewBgColor(enabled);
        final int textColor = getListItemTextColor(enabled);
        for (int i = 0; i < rootContainer.getChildCount(); i++) {
            rootContainer.getChildAt(i).setBackgroundColor(bgColor);
            ((TextView) rootContainer.getChildAt(i)).setTextColor(textColor);
        }
    }

    private int getViewBgColor(final boolean enabled) {
        return getResources().getColor(enabled ? R.color.view_bg_night : R.color.view_bg_day);
    }

    private int getListItemTextColor(final boolean enabled) {
        return getResources().getColor(enabled ? R.color.general_text_night : R.color.general_text_day);
    }

    @Override
    public final void onClick(final View v) {
        ((MainActivity) getActivity()).openChart((int) v.getTag());
    }
}
