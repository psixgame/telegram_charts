package com.psixgame.telegramcharts.entities;

import java.util.List;

public class Chart {

    private List<Series> series;

    public List<Series> getSeries() {
        return series;
    }

    public void setSeries(final List<Series> series) {
        this.series = series;
    }

    @Override
    public String toString() {
        return "Chart{" +
                "series=" + series +
                '}';
    }
}
