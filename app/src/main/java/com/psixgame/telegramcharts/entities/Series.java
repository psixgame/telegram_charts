package com.psixgame.telegramcharts.entities;

import java.util.List;

public class Series {

    public static final int TYPE_LINE = 0;
    private String name;
    private List<SeriesItem> items;
    private String color;
    private int type;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public List<SeriesItem> getItems() {
        return items;
    }

    public void setItems(final List<SeriesItem> items) {
        this.items = items;
    }

    public String getColor() {
        return color;
    }

    public void setColor(final String color) {
        this.color = color;
    }

    public int getType() {
        return type;
    }

    public void setType(final int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Series{" +
                "name='" + name + '\'' +
                ", items=" + items +
                ", color='" + color + '\'' +
                ", type=" + type +
                '}';
    }
}
