package com.psixgame.telegramcharts.entities;

public class SeriesItem {

    private final long x;
    private final long y;

    public SeriesItem(final long x, final long y) {
        this.x = x;
        this.y = y;
    }

    public long getX() {
        return x;
    }

    public long getY() {
        return y;
    }

    @Override
    public String toString() {
        return "SeriesItem{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
