package com.psixgame.telegramcharts;

import android.app.Application;

import com.psixgame.telegramcharts.settings.Settings;

public class MyApplication extends Application {

    private Settings settings;
    private DataHolder dataHolder;

    @Override
    public final void onCreate() {
        super.onCreate();

        settings = new Settings(this);
        dataHolder = new DataHolder();
    }

    public Settings getSettings() {
        return settings;
    }

    public DataHolder getDataHolder() {
        return dataHolder;
    }
}
