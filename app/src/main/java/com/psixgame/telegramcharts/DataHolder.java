package com.psixgame.telegramcharts;

import com.psixgame.telegramcharts.entities.Chart;

import java.util.List;

public class DataHolder {

    private List<Chart> charts;

    public final List<Chart> getCharts() {
        return charts;
    }

    public final void setCharts(final List<Chart> charts) {
        this.charts = charts;
    }

    public final boolean hasCharts() {
        return charts != null;
    }

    @Override
    public final String toString() {
        return "DataHolder{" +
                "charts=" + charts +
                '}';
    }
}
