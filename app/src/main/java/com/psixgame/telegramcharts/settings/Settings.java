package com.psixgame.telegramcharts.settings;

import android.content.Context;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

public class Settings {

    private final Context context;

    public Settings(final Context context) {
        this.context = context;
        readNightMode();
    }

    private boolean nightMode = false;

    public boolean isNightMode() {
        return nightMode;
    }

    public void toggleNightMode() {
        nightMode = !nightMode;
        saveNightMode();
        notifyListeners();
    }

    private static final String KEY_NIGHT_MODE = "key_night_mode";

    private void saveNightMode() {
        PreferenceManager
                .getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_NIGHT_MODE, nightMode)
                .apply();
    }

    private void readNightMode() {
        nightMode = PreferenceManager
                .getDefaultSharedPreferences(context)
                .getBoolean(KEY_NIGHT_MODE, false);
    }

    private final List<NightModeListener> nightModeListeners = new ArrayList<>();

    public final void addNightModeListener(final NightModeListener listener) {
        nightModeListeners.add(listener);
    }

    public final void removeNightModeListener(final NightModeListener listener) {
        nightModeListeners.remove(listener);
    }

    private void notifyListeners() {
        for (final NightModeListener listener : nightModeListeners) {
            listener.onNightModeChanged(nightMode);
        }
    }

    public interface NightModeListener {
        void onNightModeChanged(boolean enabled);
    }
}
