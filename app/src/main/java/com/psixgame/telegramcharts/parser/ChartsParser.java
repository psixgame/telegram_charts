package com.psixgame.telegramcharts.parser;

import com.psixgame.telegramcharts.entities.Chart;
import com.psixgame.telegramcharts.utils.Checks;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class ChartsParser {

    private final ChartParser chartParser;

    public ChartsParser() {
        chartParser = new ChartParser();
    }

    public final List<Chart> parse(final String str) throws JSONException {
        final JSONArray items = new JSONArray(str);
        final int size = items.length();

        final List<Chart> charts = new ArrayList<>(size);
        for (int i = 0; i < items.length(); i++) {
            charts.add(chartParser.parse(items.getJSONObject(i)));
        }

        Checks.checkTrue(size == charts.size());

        return charts;
    }

}
