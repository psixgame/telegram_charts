package com.psixgame.telegramcharts.parser;

import com.psixgame.telegramcharts.entities.Chart;
import com.psixgame.telegramcharts.entities.Series;
import com.psixgame.telegramcharts.entities.SeriesItem;
import com.psixgame.telegramcharts.utils.Checks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

class ChartParser {

    private static final String KEY_COLUMNS = "columns";
    private static final String KEY_TYPES = "types";
    private static final String KEY_NAMES = "names";
    private static final String KEY_COLORS = "colors";

    private JSONObject types;
    private JSONObject names;
    private JSONObject colors;

    final Chart parse(final JSONObject chartJson) throws JSONException {
        final JSONArray columns = chartJson.getJSONArray(KEY_COLUMNS);
        types = chartJson.getJSONObject(KEY_TYPES);
        names = chartJson.getJSONObject(KEY_NAMES);
        colors = chartJson.getJSONObject(KEY_COLORS);

        retrieveColumnsData(columns);

        final int seriesCount = ys.size();
        final List<Series> series = new ArrayList<>();
        for (int i = 0; i < seriesCount; i++) {
            series.add(createSeries(x, ys.get(i)));
        }

        Checks.checkTrue(series.size() == seriesCount);

        final Chart chart = new Chart();
        chart.setSeries(series);
        return chart;
    }

    private static final String KEY_X = "x";

    private JSONArray x;
    private List<JSONArray> ys;


    //todo: parse by positions? not check x string but use x position in json?
    private void retrieveColumnsData(final JSONArray columns) throws JSONException {
        //size must be the same
        final int size = columns.getJSONArray(0).length();
        for (int i = 1; i < columns.length(); i++) {
            Checks.checkTrue(size == columns.getJSONArray(i).length());
        }

        //one is x, others are ys
        final int ysCount = columns.length() - 1;
        ys = new ArrayList<>(ysCount);

        for (int i = 0; i < columns.length(); i++) {
            final JSONArray array = columns.getJSONArray(i);
            if (KEY_X.equals(getColumnId(array))) {
                x = array;
            } else {
                ys.add(array);
            }
        }

        Checks.checkNonNull(x);
        Checks.checkTrue(ys.size() == ysCount);
    }

    private String getColumnId(final JSONArray array) throws JSONException {
        return array.getString(0);
    }

    private Series createSeries(final JSONArray xColumn, final JSONArray yColumn) throws JSONException {
        Checks.checkTrue(xColumn.length() == yColumn.length());

        final String id = getColumnId(yColumn);

        final String name = names.getString(id);
        final String color = colors.getString(id);

        final String typeStr = types.getString(id);
        final int type = parseType(typeStr);
        Checks.checkNonNull(type);

        final List<SeriesItem> items = new ArrayList<>(xColumn.length() - 1);
        for (int i = 1; i < xColumn.length(); i++) {
            items.add(new SeriesItem(xColumn.getLong(i), yColumn.getLong(i)));
        }

        Checks.checkTrue(items.size() == xColumn.length() - 1);

        final Series series = new Series();
        series.setName(name);
        series.setType(type);
        series.setColor(color);
        series.setItems(items);

        return series;
    }

    private static final String TYPE_LINE = "line";

    private Integer parseType(final String typeStr) {
        switch (typeStr) {
            case TYPE_LINE: return Series.TYPE_LINE;
        }

        return null;
    }

}
