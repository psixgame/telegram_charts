package com.psixgame.telegramcharts.utils;

import java.util.Objects;

public abstract class Checks {

    public static void checkFalse(final boolean check) {
        if (check) throw new IllegalStateException();
    }

    public static void checkTrue(final boolean check) {
        if (!check) throw new IllegalStateException();
    }

    public static void checkNonNull(final Object object) {
        Objects.requireNonNull(object);
    }

}
