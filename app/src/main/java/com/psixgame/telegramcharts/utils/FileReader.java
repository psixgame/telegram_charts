package com.psixgame.telegramcharts.utils;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class FileReader {

    private final Context context;

    public FileReader(final Context context) {
        this.context = context;
    }

    public final String readFromAssetsFile(final String fileName, final int fileSize) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open(fileName), StandardCharsets.UTF_8));
            final StringBuilder builder = new StringBuilder(fileSize);
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
